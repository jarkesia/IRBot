Simple library to make things easier for Arduino newbies.

Includes 10 IR remote control signal codes from the random
controller I found from the trash, they need to be changed
in order to use the code with another remote.

Also expects there to be a motor connected to pin 6. Will
start it up with higher rotation speed and then drop back
to idle speed. Again works well with the motor I used.

IR sensor is to be connected to pins 9, 10 and 11. 11 is data,
10 is GND and 9 is 5V. Power is given through data pins and
failing to do so, especially if power is given to wrong pins
will most likely cause sparks. waitIR-method automatically
takes care of the power as well.
