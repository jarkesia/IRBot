/* IRBot.h from IRBot an Arduino library for simple infrared bot
 * Copyright 2015 by Jari Suominen
 * 
 * Requires IRLib library to be installed!
 */

#ifndef IRBot_h
#define IRBot_h
#include <Arduino.h>
#include "../IRLib/IRLib.h"


class IRBot
{
public:
	IRBot() : myReceiver(11) {};
	void walk();
	void stop();
	void waitIR(int code);
	void sendIR(int code);

	IRrecv myReceiver;
	IRdecode myDecoder;
	IRsend mySender;

	const static long codes[];
	
};

#endif //IRLib_h
