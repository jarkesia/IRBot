#include <Arduino.h>
#include "IRBot.h"

/* Buttons from 0 to 9 of my random remote, you need to tweak these
 * if using another one.
 */
const long IRBot::codes[] = {
	0xFF728D,
	0xFF7A85,
	0xFFBA45,
	0xFF7887,
	0xFF52AD,
	0xFF926D,
	0xFF50AF,
	0xFF5AA5,
	0xFF9A65,
	0xFF58A7
};

void IRBot::walk() {
	pinMode(6,OUTPUT);
	digitalWrite(6,HIGH);
	delay(500);
	analogWrite(6,180);  
}

void IRBot::stop() {
	digitalWrite(6,LOW);  
}

void IRBot::sendIR(int code) {
	pinMode(5,OUTPUT);
	digitalWrite(5,LOW);
	mySender.send(NEC,codes[code],32);
}

void IRBot::waitIR(int code) {
	pinMode(9,OUTPUT);
	pinMode(10,OUTPUT);
	digitalWrite(9,HIGH);
	digitalWrite(10,LOW);

	myReceiver.enableIRIn();

	while (true) {
		if (myReceiver.GetResults(&myDecoder)) {
			myDecoder.decode();
			if (myDecoder.value==codes[code])
				return;
			myReceiver.resume(); 
		}    
	}
}

