/* 
 * Example program for from IRBot – IR controlled vibrabot
 * Version 0.9   May 2015
 * Copyright 2015 by Jari Suominen
 */

#include <IRLib.h>
#include <IRBot.h>

IRBot bot;

void setup() {

}

void loop() {
  pinMode(13,OUTPUT);
  bot.waitIR(3);
  bot.walk();
  digitalWrite(13,HIGH);
  bot.waitIR(0);
  bot.stop();
  digitalWrite(13,LOW);
}

